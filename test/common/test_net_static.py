from assemblyline.common.net_static import TLDS_ALPHA_BY_DOMAIN
import requests
import pytest


def test_tlds_update():
    r = requests.get('https://data.iana.org/TLD/tlds-alpha-by-domain.txt')
    if r.status_code != 200:
        pytest.skip("Can't reach iana.org to check domain list.")

    tlds = r.text.splitlines()

    tlds = [domain for domain in tlds if domain.strip()]
    tlds = [domain for domain in tlds if len(domain) > 0]
    tlds = [domain for domain in tlds if domain[0] != '#']
    tlds = set(tlds)

    missing = tlds - set(TLDS_ALPHA_BY_DOMAIN)
    extra = set(TLDS_ALPHA_BY_DOMAIN) - tlds

    assert len(missing) == 0, "We are missing top level domains from iana.org"
    assert len(extra) == 0, "We have domains no longer listed by iana.org"
