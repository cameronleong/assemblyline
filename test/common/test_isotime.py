
from time import time
from assemblyline.common.isotime import now, now_as_iso, now_as_local, local_to_epoch, iso_to_epoch, epoch_to_local, epoch_to_iso, utc_offset_from_local, iso_to_local, local_to_iso


def test_time_conversions():

    # my_time = time()
    # print "\nCurrent Epoch:", my_time
    # print "Now as Epoch:", now(), "\n"
    #
    # print "Now as ISO:", now_as_iso()
    # print "Now as Local:", now_as_local(), "\n"

    temp_time = time()
    print "Testing conversion functions with time:", temp_time
    assert abs(iso_to_epoch(local_to_iso(epoch_to_local(temp_time))) - temp_time) < 0.000001, "Local -> ISO"
    assert abs(local_to_epoch(iso_to_local(epoch_to_iso(temp_time))) - temp_time) < 0.000001, "ISO -> Local"

    my_time = time()
    print "Testing functions with time:", my_time
    assert my_time == iso_to_epoch(epoch_to_iso(my_time)), "\tISO functions:"
    assert abs(my_time - local_to_epoch(epoch_to_local(my_time))) < 0.000001
    print "\tUTC offset from local:", utc_offset_from_local(my_time), "\n"

    my_time = local_to_epoch("2015-01-01 00:00:00")
    print "Testing functions with time (NOT daylight savings time):", my_time
    assert my_time == iso_to_epoch(epoch_to_iso(my_time)), "\tISO functions:"
    assert abs(my_time - local_to_epoch(epoch_to_local(my_time))) < 0.000001, "Local Functions"
    print "\tUTC offset from local:", utc_offset_from_local(my_time), "\n"

    my_time = local_to_epoch("2015-05-05 00:00:00")
    print "Testing functions with time (Daylight savings time):", my_time
    assert my_time == iso_to_epoch(epoch_to_iso(my_time)), "\tISO functions:"
    assert abs(my_time - local_to_epoch(epoch_to_local(my_time))) < 0.000001, "Local Functions"
    print "\tUTC offset from local:", utc_offset_from_local(my_time)
