#!/usr/bin/env python

import logging
import time

from assemblyline.al.common import forge
from assemblyline.al.common import log
from assemblyline.al.common.task import Task
from assemblyline.al.service.list_queue_sizes import get_service_queue_lengths

log.init_logging('plumber')
logger = logging.getLogger('assemblyline.plumber')

dispatch_queue = forge.get_dispatch_queue()
store = forge.get_datastore()
config = forge.get_config()
service_queue = {}


def get_queue(n):
    q = service_queue.get(n, None)
    if not q:
        service_queue[n] = q = forge.get_service_queue(n)

    return q


def get_service_max_queue_sizes():
    default_mqs = config.core.get('plumber', {}).get('max_queue_size', None)
    temp_mqs_map = {}

    for service in store.list_services():
        # noinspection PyBroadException
        try:
            svc_name = service.get('name')
            params = service.get('config', {})
            if service.get('enabled', False):
                mqs = params.get('PLUMBER_MAX_QUEUE_SIZE', default_mqs)
            else:
                mqs = 0

            if mqs is not None:
                temp_mqs_map[svc_name] = mqs
        except Exception:  # pylint:disable=W0702
            logger.exception('Problem getting service config:')

    return temp_mqs_map


if __name__ == "__main__":
    logger.info("Monitoring all services for unexpected queue lengths...")

    while True:
        mqs_map = get_service_max_queue_sizes()
        queue_lengths = get_service_queue_lengths()

        over = {
            k: v for k, v in queue_lengths.iteritems() if k in mqs_map and v > mqs_map[k]
        }

        for name, size in over.iteritems():
            excess = size - mqs_map.get(name, size)
            if excess <= 0:
                continue

            for msg in get_queue(name).unpush(excess):
                # noinspection PyBroadException
                try:
                    t = Task(msg)

                    t.watermark(name, '')
                    t.nonrecoverable_failure('Service busy.')
                    t.cache_key = store.save_error(name, None, None, t)

                    dispatch_queue.send_raw(t.as_dispatcher_response())
                    logger.info("%s is too busy to process %s.", name, t.srl)
                except Exception:  # pylint:disable=W0702
                    logger.exception('Problem sending response:')

        time.sleep(config.system.update_interval)
