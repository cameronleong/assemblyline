#!/usr/bin/env python
from assemblyline.al.common.config_riak import load_seed, save_seed
from assemblyline.al.common.service_utils import infer_classpath
from assemblyline.al.service import register_service


def install(alsi=None):
    # shortly we will allow node specific service list override. 
    # for now they always get default.
    services_to_register = alsi.config['services']['master_list']
    alsi.info("Preparing to Register: %s", ",".join(services_to_register.keys()))

    for service, svc_detail in services_to_register.iteritems():
        # noinspection PyBroadException
        try:
            svc_detail = alsi.config['services']['master_list'][service]
            classpath = infer_classpath(svc_detail)
            config_overrides = svc_detail.get('config', {})

            register_service.register(classpath, config_overrides=config_overrides,
                                      enabled=svc_detail.get('enabled', True))
        except Exception:
            alsi.fatal("Failed to register service %s." % service)
            alsi.log.exception('While registering service %s', service)

    seed = load_seed()
    save_seed(seed, "original_seed")
    save_seed(seed, "previous_seed")


if __name__ == '__main__':
    from assemblyline.al.install import SiteInstaller
    installer = SiteInstaller()
    install(installer)
